extends "res://actors/Actor.gd"

onready var raycast := $RayCast2D

func set_direction(_direction: Vector2) -> void:
	.set_direction(_direction)
	raycast.cast_to = direction * Globals.TILE_SIZE

func trigger_with_touch() -> bool:
	   for trigger_area in area.get_overlapping_areas():
			   if trigger_area.has_method("activate_trigger") and trigger_area.get("trigger_type") == TriggerArea.TriggerType.PLAYER_TOUCH:
					   trigger_area.activate_trigger(self)
					   return true
	   
	   return false

func trigger_with_action_button() -> bool:
	   raycast.force_raycast_update()
	   var collider = raycast.get_collider()
	   if collider != null and collider.has_method("activate_trigger") and collider.get("trigger_type") == TriggerArea.TriggerType.ACTION_BUTTON:
			   collider.activate_trigger(self)
			   return true
	   return false
