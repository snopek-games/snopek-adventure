extends "res://actors/Actor.gd"

signal trigger_activated (actor)

func _on_TriggerArea_trigger_activated(actor) -> void:
	var vector = actor.tile_position - tile_position
	turn(vector)
	
	emit_signal("trigger_activated", actor)
