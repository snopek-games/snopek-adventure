extends Node2D

onready var sprite := $Sprite
onready var area := $Area2D
onready var animation_player := $AnimationPlayer
onready var animation_tree := $AnimationTree
onready var animation_state: AnimationNodeStateMachinePlayback = animation_tree.get("parameters/playback")
onready var tween := $Tween

export (Texture) var sprite_texture = preload("res://assets/people/female/F_07.png") setget set_sprite_texture

const WALK_TIME := 0.4

var tile_position: Vector2 = Vector2.ZERO setget set_tile_position
var direction: Vector2 = Vector2.DOWN setget set_direction
var in_progress := false

signal completed_action ()

func _ready() -> void:
	tile_position = Vector2(position.x / Globals.TILE_SIZE, position.y / Globals.TILE_SIZE)
	sprite.texture = sprite_texture

func set_sprite_texture(_texture: Texture) -> void:
	if sprite_texture != _texture:
		sprite_texture = _texture
		if sprite != null:
			sprite.texture = _texture

func set_tile_position(_tile_position: Vector2) -> void:
	if tile_position != _tile_position:
		tile_position = _tile_position
		position = tile_position * Globals.TILE_SIZE

func set_direction(_direction: Vector2) -> void:
	if direction != _direction:
		direction = _direction
		animation_tree.set("parameters/Stand/blend_position", direction)
		animation_tree.set("parameters/Walk/blend_position", direction)

func move(vector: Vector2) -> void:
	assert(!in_progress)
	assert(Utils.is_cardinal_vector(vector))
	
	if in_progress:
		return
	
	set_direction(vector)
	in_progress = true
	tile_position += vector
	animation_state.travel("Walk")
	tween.interpolate_property(self, "position", position, tile_position * Globals.TILE_SIZE,
		WALK_TIME, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()

func turn(vector: Vector2) -> void:
	assert(!in_progress)
	assert(Utils.is_cardinal_vector(vector))
	
	if in_progress:
		return
	
	set_direction(vector)
	animation_state.travel("Stand")

func _on_Tween_tween_all_completed() -> void:
	animation_state.travel("Stand")
	in_progress = false
	emit_signal("completed_action")

