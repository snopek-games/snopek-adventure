extends Node2D
class_name Game

onready var player := $Player
onready var map := $Map
onready var dialog_box := $CanvasLayer/DialogBox
onready var fade := $CanvasLayer/Fade

var player_control_enabled := true

var last_choice_index := -1
var last_choice_text := ""

var plotted_path := []
var direction_after_path := Vector2.ZERO
var trigger_action_after_path := false

enum ActionType {
	MESSAGE,
}

signal completed_action (info)

func _ready() -> void:
	GameState.setup(self)
	VisualServer.set_default_clear_color(Color(0.0, 0.0, 0.0, 1.0))
	player.connect("completed_action", self, "_on_Player_completed_action")
	
	Music.play("GameMusic")
	if map.has_method("start_map"):
		map.start_map()

func _unhandled_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and not event.pressed:
		direction_after_path = Vector2.ZERO
		trigger_action_after_path = false
		
		var tile_position: Vector2 = (get_global_mouse_position() / Globals.TILE_SIZE).floor()
		var tile_position_delta = tile_position - player.tile_position
		
		# Special case for turning and trying to activate an unwalkable tile
		# that we're right next to, without walking all over the place unnecessarily.
		if Utils.is_cardinal_vector(tile_position_delta) and !map.is_unobstructed_walkable_tile(tile_position):
			if player.in_progress:
				plotted_path = []
				direction_after_path = tile_position_delta
				trigger_action_after_path = true
			else:
				player.turn(tile_position_delta)
				trigger_with_action_button()
			return
		
		plotted_path = map.plot_path(player.tile_position, tile_position)
		if plotted_path.size() == 0:
			var closest_tile_position = map.get_closest_tile(tile_position)
			var closest_tile_delta = tile_position - closest_tile_position
			
			plotted_path = map.plot_path(player.tile_position, closest_tile_position)
			if plotted_path.size() > 0:
				direction_after_path = closest_tile_delta.normalized()
				if not Utils.is_cardinal_vector(direction_after_path):
					direction_after_path = Vector2.ZERO
				elif closest_tile_position + direction_after_path == tile_position or closest_tile_position + (direction_after_path * 2) == tile_position:
					trigger_action_after_path = true
			# If we're standing right next to the tile we just clicked, then
			# turn towards it and trigger with action button.
			elif Utils.is_cardinal_vector(closest_tile_delta.normalized()) && closest_tile_delta.length() <= 2.0:
				closest_tile_delta = closest_tile_delta.normalized()
				# I think this code can trigger when the player is still
				# in progress of moving, in which case we can't just turn
				# and trigger, we need to wait until after they're done.
				if player.in_progress:
					direction_after_path = closest_tile_delta
					trigger_action_after_path = true
				else:
					player.turn(closest_tile_delta)
					trigger_with_action_button()
		
		# Start moving on plotted path if the player isn't already moving
		if plotted_path.size() > 0 && !player.in_progress:
			try_move(plotted_path.pop_front())
	if event.is_action_released("player_action"):
		trigger_with_action_button()
		get_tree().set_input_as_handled()

func _process(delta: float) -> void:
	if player_control_enabled && !player.in_progress && plotted_path.size() == 0:
		if Input.is_action_pressed("player_up"):
			try_move(Vector2.UP)
		elif Input.is_action_pressed("player_down"):
			try_move(Vector2.DOWN)
		elif Input.is_action_pressed("player_left"):
			try_move(Vector2.LEFT)
		elif Input.is_action_pressed("player_right"):
			try_move(Vector2.RIGHT)

func do_action(info: Dictionary) -> void:
	assert(info.has("type"))
	
	player_control_enabled = false
	
	match info["type"]:
		ActionType.MESSAGE:
			assert(info.has("message"))
			dialog_box.show_dialog(info.get("message", ""), info.get("name"), info.get("choices", []))
			yield(dialog_box, "dismissed")
	
	player_control_enabled = true
	emit_signal("completed_action", info)

func try_move(vector: Vector2) -> void:
	if map.is_unobstructed_walkable_tile(player.tile_position + vector):
		player.move(vector)
	else:
		player.turn(vector)

func trigger_with_action_button() -> bool:
	var result = player.trigger_with_action_button()
	if result:
		Sound.play("ConfirmSound")
	return result

func _on_Player_completed_action():
	if player.trigger_with_touch():
		plotted_path = []
	
	if plotted_path.size() > 0:
		try_move(plotted_path.pop_front())
	else:
		if direction_after_path != Vector2.ZERO:
			player.turn(direction_after_path)
			if trigger_action_after_path:
				trigger_with_action_button()
			direction_after_path = Vector2.ZERO

func _on_DialogBox_dismissed(choice_index, choice_text) -> void:
	last_choice_index = choice_index
	last_choice_text = choice_text
