extends Button

onready var selected_sprite := $SelectedSprite

var skip_sound := false

func _notification(what: int) -> void:
	match what:
		NOTIFICATION_FOCUS_ENTER:
			selected_sprite.visible = true
			if skip_sound:
				skip_sound = false
			else:
				Sound.play("CursorSound")
		
		NOTIFICATION_FOCUS_EXIT:
			selected_sprite.visible = false
		
		NOTIFICATION_MOUSE_ENTER:
			grab_focus()

func grab_focus_without_sound() -> void:
	skip_sound = true
	grab_focus()

func _on_MenuButton_pressed() -> void:
	Sound.play("ConfirmSound")
