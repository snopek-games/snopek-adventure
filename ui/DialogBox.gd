extends Control

onready var name_panel := $VBoxContainer/HBoxContainer/NamePanel
onready var name_label := $VBoxContainer/HBoxContainer/NamePanel/NameLabel
onready var message_label := $VBoxContainer/MessagePanel/MessageLabel
onready var choice_panel := $VBoxContainer/HBoxContainer/ChoicePanel
onready var animation_player := $AnimationPlayer
onready var tween := $Tween

const CHARACTERS_PER_SECOND := 20

var has_choices := false
var animating := false

signal dismissed (choice_index, choice_text)

func show_dialog(message: String, name = null, _choices = []) -> void:
	if visible:
		return
	
	visible = true
	
	if name != null:
		name_panel.visible = true
		name_label.text = name
	else:
		name_panel.visible = false
	
	choice_panel.visible = false
	if _choices.size() > 0:
		has_choices = true
		choice_panel.choices = _choices
	else:
		has_choices = false
	
	message_label.text = message
	message_label.percent_visible = 0.0
	tween.interpolate_property(message_label, "percent_visible", 0.0, 1.0, max(message.length() / CHARACTERS_PER_SECOND, 0.5))
	tween.start()
	
	animation_player.play("HideArrow")
	
	animating = true

func _input(event: InputEvent) -> void:
	# If we're not visible, don't do anything.
	if !visible:
		return
	
	# If the choices are currently displayed, don't do anything.
	if choice_panel.visible:
		return
	
	if event.is_action_released("ui_accept") or (event is InputEventMouseButton && event.button_index == BUTTON_LEFT && !event.pressed):
		get_tree().set_input_as_handled()
		if animating:
			tween.remove_all()
			_on_Tween_tween_all_completed()
		elif not has_choices:
			_dismiss_dialog()

func _dismiss_dialog() -> void:
	if visible:
		visible = false
		animation_player.play("HideArrow")
		Sound.play("CursorSound")
		emit_signal("dismissed", -1, "")

func _on_Tween_tween_all_completed() -> void:
	if animating:
		message_label.percent_visible = 1.0
		animating = false
		
		if has_choices:
			choice_panel.visible = true
			choice_panel.focus_first_choice()
		else:
			animation_player.play("BounceArrow")

func _on_ChoicePanel_chosen(index, text) -> void:
	visible = false
	emit_signal("dismissed", index, text)
