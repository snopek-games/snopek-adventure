extends PanelContainer

onready var back_button := $VBoxContainer/HBoxContainer/BackButton

signal back ()

func show_screen() -> void:
	back_button.grab_focus_without_sound()

func _on_BackButton_pressed() -> void:
	emit_signal("back")
