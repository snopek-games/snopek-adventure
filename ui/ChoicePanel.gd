extends PanelContainer

onready var choice_container = $ChoiceContainer

export (Array) var choices = ["Yes", "No"] setget set_choices

var MenuButton = preload("res://ui/MenuButton.tscn")

signal chosen (index, text)

func set_choices(_choices: Array) -> void:
	if choices == _choices:
		return
	
	choices = _choices
	if choice_container != null:
		_rebuild_buttons()

func _ready() -> void:
	# Rebuild with the real buttons.
	_rebuild_buttons()

func _rebuild_buttons() -> void:
	for index in range(choices.size()):
		var button: Button
		if index < choice_container.get_child_count():
			button = choice_container.get_child(index)
			if button.text != choices[index]:
				button.text = choices[index]
		else:
			button = MenuButton.instance()
			button.name = str(index)
			button.text = choices[index]
			choice_container.add_child(button)
		if not button.is_connected("pressed", self, "_on_Button_pressed"):
			button.connect("pressed", self, "_on_Button_pressed", [index])
	while choice_container.get_child_count() > choices.size():
		var child = choice_container.get_child(choices.size())
		if child == null:
			break
		choice_container.remove_child(child)
		child.queue_free()
	
	# Shrink the panel and force it to expand itself to fit its children
	rect_size = rect_min_size

func focus_first_choice(skip_sound = true) -> void:
	var child = choice_container.get_child(0)
	if child != null:
		child.grab_focus_without_sound()

func _on_Button_pressed(index) -> void:
	# Prevent the game from using the input (mouse click or key press)
	get_tree().set_input_as_handled()
	
	emit_signal("chosen", index, choices[index])


