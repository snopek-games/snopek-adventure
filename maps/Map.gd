extends Node2D

onready var tilemaps_parent = $Tilemaps
onready var objects_parent = $Objects
onready var actors_parent = $Actors

var used_rect = Rect2()
var collision_tilemaps := []
var walkable_tilemaps := []
var astar := AStar2D.new()

func _ready() -> void:
	for child in tilemaps_parent.get_children():
		if child is TileMap:
			used_rect = used_rect.merge(child.get_used_rect())
			if child.has_meta("collision") and bool(child.get_meta("collision")):
				collision_tilemaps.append(child)
			elif child.z_index <= 0:
				walkable_tilemaps.append(child)
	_setup_astar()

func is_collision_tile(tile_position: Vector2) -> bool:
	# DRS: I'm really not sure why this is necessary. The Y position just seems
	#      to be off by one per what I'd expect from the math...
	tile_position.y += 1
	
	for tilemap in collision_tilemaps:
		if tilemap.get_cellv(tile_position) != -1:
			return true
	return false

func is_walkable_tile(tile_position: Vector2) -> bool:
	if is_collision_tile(tile_position):
		return false
	
	tile_position.y += 1
	
	for tilemap in walkable_tilemaps:
		if tilemap.get_cellv(tile_position) != -1:
			return true
	
	return false

func is_unobstructed_walkable_tile(tile_position: Vector2) -> bool:
	if !is_walkable_tile(tile_position):
		return false
	
	for actor in actors_parent.get_children():
		var actor_tile_position = actor.get("tile_position")
		if actor_tile_position != null and tile_position == actor_tile_position:
			return false
	
	return true

func _tile_position_to_astar_id(tile_position: Vector2) -> int:
	return (tile_position.y * used_rect.size.x) + tile_position.x

func _astar_connect_if_exists(astar_id: int, other_tile_position: Vector2) -> void:
	var other_astar_id: int = _tile_position_to_astar_id(other_tile_position)
	if astar.has_point(other_astar_id):
		astar.connect_points(astar_id, other_astar_id)

func _setup_astar() -> void:
	for x in range(used_rect.position.x, used_rect.size.x):
		for y in range(used_rect.position.x, used_rect.size.y):
			var tile_position := Vector2(x, y)
			if is_walkable_tile(tile_position):
				astar.add_point(_tile_position_to_astar_id(tile_position), tile_position)
	
	for x in range(used_rect.position.x, used_rect.size.x):
		for y in range(used_rect.position.y, used_rect.size.y):
			var tile_position := Vector2(x, y)
			var astar_id = _tile_position_to_astar_id(tile_position)
			if astar.has_point(astar_id):
				_astar_connect_if_exists(astar_id, tile_position + Vector2.RIGHT)
				_astar_connect_if_exists(astar_id, tile_position + Vector2.DOWN)

func plot_path(start: Vector2, end: Vector2, disable_points: Array = []) -> Array:
	var start_id = _tile_position_to_astar_id(start)
	var end_id = _tile_position_to_astar_id(end)
	
	# Loop over all the astar points and enable them.
	for astar_id in astar.get_points():
		astar.set_point_disabled(astar_id, false)
	
	# Loop over all the actors on the map, and disable the astar points they
	# are standing on.
	for child in actors_parent.get_children():
		var tile_position = child.get("tile_position")
		if tile_position != null:
			var astar_id = _tile_position_to_astar_id(child.tile_position)
			if astar.has_point(astar_id):
				astar.set_point_disabled(astar_id, true)
	
	# And disable the requested points just for this call.
	for disable_point in disable_points:
		var astar_id = _tile_position_to_astar_id(disable_point)
		if astar.has_point(astar_id):
			astar.set_point_disabled(astar_id, true)
	
	var path = []
	var last_point = start
	for point in astar.get_point_path(start_id, end_id):
		if point == start:
			continue
		var vector = point - last_point
		last_point = point
		path.append(vector)
	
	return path

func get_closest_tile(tile_position: Vector2):
	if is_unobstructed_walkable_tile(tile_position):
		return tile_position
	
	# Attempt to favor positions directly below the tile position
	if is_unobstructed_walkable_tile(tile_position + Vector2.DOWN):
		return tile_position + Vector2.DOWN
	
	var astar_id = astar.get_closest_point(tile_position)
	if astar_id == -1:
		return null
	var point_position = astar.get_point_position(astar_id)
	
	# Attempt to favor positions 2 tiles below the tile position, if astar
	# gives us a point that's 2 or more away.
	if tile_position.distance_to(point_position) >= 2.0:
		if is_unobstructed_walkable_tile(tile_position + (Vector2.DOWN * 2.0)):
			return tile_position + (Vector2.DOWN * 2.0)
		
	return point_position

# Helper function since maps show a lot of messages.
func show_message(name, message: String, choices: Array = []) -> void:
	GameState.game.do_action({
		type = Game.ActionType.MESSAGE,
		name = name,
		message = message,
		choices = choices,
	})
