extends "res://maps/Map.gd"

func _on_TriggerArea_trigger_activated(actor) -> void:
	GameState.game.do_action({
		type = Game.ActionType.MESSAGE,
		name = "Narrator",
		message = "Careful! The oven is hot!"
	})

func _on_TriggerArea2_trigger_activated(actor) -> void:
	GameState.game.do_action({
		type = Game.ActionType.MESSAGE,
		name = "Narrator",
		message = "Welcome to the house!",
	})
