extends Node

var map_script := preload("res://maps/Map.gd")

func post_import(scene: Node2D):
	var tilemaps := Node2D.new()
	tilemaps.name = "Tilemaps"
	
	var objects := Node2D.new()
	objects.name = "Objects"
	
	for child in scene.get_children():
		if child.has_meta('z_index'):
			child.z_index = int(child.get_meta('z_index'))
		scene.remove_child(child)
		if child is TileMap:
			tilemaps.add_child(child)
		else:
			objects.add_child(child)
	
	scene.add_child(tilemaps)
	scene.add_child(objects)
	
	var actors := Node2D.new()
	actors.name = "Actors"
	scene.add_child(actors)
	
	# Make sure all nodes have the scene as the owner, so that they'll get
	# saved correctly by PackedScene.pack().
	for child in scene.get_children():
		child.owner = scene
	for child in tilemaps.get_children():
		child.owner = scene
	for child in objects.get_children():
		child.owner = scene
	
	scene.set_script(map_script)
	
	return scene
