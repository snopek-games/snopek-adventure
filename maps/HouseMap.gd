extends "res://maps/Map.gd"

onready var sasza = $Actors/Sasza
onready var ewa = $Actors/Ewa

var sasza_plotted_path := []
var sasza_steps := 0

var ewa_plotted_path := []

func start_map() -> void:
	var intro_messages = [
		"Ewa is exploring the house, looking for adventure...",
		"[Use WASD or ARROW KEYS for direct movement\n and ENTER to interact]",
		"[Or click on stuff for both movement and interaction!]",
	]
	
	for message in intro_messages:
		show_message(null, message)
		yield(GameState.game, "completed_action")

func _on_Sasza_trigger_activated(actor) -> void:
	if not GameState.variables.get("sasza_playing", false):
		show_message("Sasza", "Hey Ewa! Do you want to play?", ["Yes", "No"])
		yield(GameState.game, "completed_action")
		
		if GameState.game.last_choice_index == 0:
			show_message("Sasza", "Awesome!")
			yield(GameState.game, "completed_action")
			show_message("Sasza", "Let's play chase!\nYou can't catch me!!!")
			yield(GameState.game, "completed_action")
			
			GameState.variables["sasza_playing"] = true
			GameState.game.player_control_enabled = false
			sasza_plotted_path = GameState.game.map.plot_path(sasza.tile_position, Vector2(7, 15), [GameState.game.player.tile_position])
			sasza.move(sasza_plotted_path.pop_front())
		else:
			show_message("Sasza", "Aw! I really wanted to play...")
	else:
		show_message("Sasza", "You caught me!")
		yield(GameState.game, "completed_action")
		show_message("Ewa", "...")
		yield(GameState.game, "completed_action")
		show_message("Ewa", "Oh, no!")
		yield(GameState.game, "completed_action")
		show_message("Sasza", "What?")
		yield(GameState.game, "completed_action")
		show_message("Ewa", "Where's my bunny?")
		yield(GameState.game, "completed_action")
		
		GameState.variables["looking_for_bunny"] = true
		
		# Make it appear as though the player has become Sasza
		var player = GameState.game.player
		ewa.tile_position = player.tile_position
		ewa.direction = player.direction
		ewa.visible = true
		player.tile_position = sasza.tile_position
		player.direction = sasza.direction
		player.sprite_texture = sasza.sprite_texture
		sasza.tile_position = Vector2(0, 0)
		sasza.visible = false
		
		ewa_plotted_path = GameState.game.map.plot_path(ewa.tile_position, Vector2(27, 11), [GameState.game.player.tile_position])
		ewa.move(ewa_plotted_path.pop_front())

func _on_Ewa_completed_action() -> void:
	if ewa_plotted_path.size() > 0:
		ewa.move(ewa_plotted_path.pop_front())

func _on_Sasza_completed_action() -> void:
	if sasza_plotted_path.size() > 0:
		sasza_steps += 1
		sasza.move(sasza_plotted_path.pop_front())
		
		if sasza_steps == 3:
			GameState.game.player.turn(Vector2.LEFT)
			show_message("Ewa", "Yes, I can!")

func _on_Ewa_trigger_activated(actor) -> void:
	if GameState.variables.get("bunny_found", false):
		show_message("Sasza", "Here's your bunny!")
		yield(GameState.game, "completed_action")
		show_message("Ewa", "You found it!")
		yield(GameState.game, "completed_action")
		show_message("Ewa", "You're the best big sister ever!")
		yield(GameState.game, "completed_action")
		
		var outro_messages = [
			"[Sorry, that's all there is for now!]",
			"[We hope to add many more adventures in the future...]",
			"[Bye bye!]",
		]
	
		for message in outro_messages:
			show_message(null, message)
			yield(GameState.game, "completed_action")
		
		GameState.game.fade.fade_out()
		yield(GameState.game.fade, "fade_out_finished")
		
		get_tree().change_scene("res://TitleScreen.tscn")
	else:
		show_message("Ewa", "WHERE IS MY BUNNY?!")
		yield(GameState.game, "completed_action")
		show_message("Sasza", "I'll find it for you!")
		yield(GameState.game, "completed_action")
		ewa.turn(Vector2.UP)

func _no_bunny_here() -> void:
	if GameState.variables.get("looking_for_bunny", false) and not GameState.variables.get("bunny_found", false):
		yield(GameState.game, "completed_action")
		show_message(null, "No bunny here...")

func _on_OvenTrigger_trigger_activated(actor) -> void:
	show_message(null, "Careful! The oven is hot!")
	_no_bunny_here()

func _on_SinkTrigger_trigger_activated(actor) -> void:
	show_message(null, "Good job washing your hands!")
	_no_bunny_here()

func _on_TableTrigger_trigger_activated(actor) -> void:
	show_message(null, "I can't wait until dinner... and then DESSERT!")
	_no_bunny_here()

func _on_PhotoTrigger_trigger_activated(actor) -> void:
	show_message(null, "Pictures of Mama and Tata from olden times.")

func _on_ClockTrigger_trigger_activated(actor) -> void:
	show_message(null, "Coockoo! Coockoo!")

func _on_PlantTrigger1_trigger_activated(actor) -> void:
	show_message(null, "It's like a jungle in there!")
	_no_bunny_here()

func _on_PianaTrigger_trigger_activated(actor) -> void:
	var messages = [
		"Hot, cross, buns...\nHot, cross, buns...",
		"Do a diddy, do a diddy...",
		"Hot, cross, buns...",
	]
	for message in messages:
		show_message(null, message)
		yield(GameState.game, "completed_action")

func _on_CupboardTrigger1_trigger_activated(actor) -> void:
	show_message(null, "Forks and Spoons and Knives...")
	_no_bunny_here()

func _on_CupboardTrigger2_trigger_activated(actor) -> void:
	show_message(null, "Sugar and Flour and Coffee...")
	_no_bunny_here()

func _on_CupboardTrigger3_trigger_activated(actor) -> void:
	show_message(null, "Oil and Eggs and Butter...")
	_no_bunny_here()

func _on_CupboardTrigger4_trigger_activated(actor) -> void:
	show_message(null, "Lots of fun things in here that are NOT toys...")
	_no_bunny_here()

func _on_CupboardTrigger5_trigger_activated(actor) -> void:
	show_message(null, "What even is this cooking stuff for?")
	
	if GameState.variables.get("looking_for_bunny", false) and not GameState.variables.get("bunny_found", false):
		yield(GameState.game, "completed_action")
		show_message("Sasza", "...")
		yield(GameState.game, "completed_action")
		show_message("Sasza", "What's Ewa's bunny doing in here!")
		yield(GameState.game, "completed_action")
		show_message(null, "* You found EWA'S SPECIAL BUNNY! *")
		
		GameState.variables["bunny_found"] = true

func _on_FireTrigger_trigger_activated(actor) -> void:
	show_message(null, "I like to watch the fire!\nIt's only a little scary...")
	_no_bunny_here()

func _on_BookshelfTrigger_trigger_activated(actor) -> void:
	show_message(null, "Here's all my favorite story books!")
	yield(GameState.game, "completed_action")
	show_message(null, "Read a book?", [
		"No, thanks",
		"\"Blippi Bed Time\"",
		"\"We're in a book!\"",
	])
	yield(GameState.game, "completed_action")
	if GameState.game.last_choice_index > 0:
		show_message(null, "Reading " + GameState.game.last_choice_text + "...")
		yield(GameState.game, "completed_action")
	_no_bunny_here()

func _on_ToyboxTrigger_trigger_activated(actor) -> void:
	show_message(null, "This is where I keep all my toys!")
	_no_bunny_here()

func _on_NightstandTrigger_trigger_activated(actor) -> void:
	show_message(null, "This is where I hide all my special things...")
	_no_bunny_here()

func _on_MakeupTrigger_trigger_activated(actor) -> void:
	show_message(null, "Should I put on my makeup for the day?", ["Yes", "No"])
	yield(GameState.game, "completed_action")
	
	if GameState.game.last_choice_index == 0:
		show_message(null, "Now I look beautiful!")

func _on_BedroomPictureTrigger_trigger_activated(actor) -> void:
	show_message(null, "This picture is weird...\nBut I like it!")

func _on_BedTrigger1_trigger_activated(actor) -> void:
	show_message(null, "It's not sleepy time!")
	_no_bunny_here()

func _on_LeaveTrigger_trigger_activated(actor) -> void:
	show_message(null, "It's not time to go outside!")
	yield(GameState.game, "completed_action")
	actor.move(Vector2.UP)

