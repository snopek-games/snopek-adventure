extends Node

func is_cardinal_vector(vector: Vector2) -> bool:
	if vector == Vector2.UP or vector == Vector2.DOWN or vector == Vector2.LEFT or vector == Vector2.RIGHT:
		return true
	return false
