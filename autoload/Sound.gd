extends Node

func play(name: String) -> void:
	var node = get_node(name)
	if node is AudioStreamPlayer:
		node.play()

