extends Area2D
class_name TriggerArea

enum TriggerType {
	ACTION_BUTTON,
	PLAYER_TOUCH,
}

export (TriggerType) var trigger_type := TriggerType.ACTION_BUTTON

signal trigger_activated (actor)

func activate_trigger(actor) -> void:
	emit_signal("trigger_activated", actor)

