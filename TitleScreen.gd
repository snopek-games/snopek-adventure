extends Node2D

onready var sasza := $Sasza
onready var ewa := $Ewa
onready var choice_panel := $CanvasLayer/ChoicePanel
onready var credits_screen := $CanvasLayer/CreditsScreen
onready var fade := $CanvasLayer/Fade

var sasza_direction := Vector2.RIGHT
var ewa_direction := Vector2.RIGHT
var sasza_moves := 0
var ewa_moves := 0

const MOVE_LIMIT = 24

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	VisualServer.set_default_clear_color(Color(0.0, 0.0, 0.0, 1.0))
	choice_panel.focus_first_choice()
	_on_Sasza_completed_action()
	_on_Ewa_completed_action()
	Music.play("TitleMusic")
	fade.fade_in()

func _on_Sasza_completed_action() -> void:
	sasza_moves += 1
	sasza.move(sasza_direction)
	if sasza_moves > MOVE_LIMIT:
		sasza_moves = 0
		sasza_direction = -sasza_direction

func _on_Ewa_completed_action() -> void:
	ewa_moves += 1
	ewa.move(ewa_direction)
	if ewa_moves > MOVE_LIMIT:
		ewa_moves = 0
		ewa_direction = -ewa_direction

func _on_ChoicePanel_chosen(index, text) -> void:
	match index:
		0:
			fade.fade_out()
			yield(fade, "fade_out_finished")
			get_tree().change_scene("res://Game.tscn")
		
		1:
			credits_screen.show()
			credits_screen.show_screen()
			yield(credits_screen, "back")
			credits_screen.hide()
			choice_panel.focus_first_choice()

